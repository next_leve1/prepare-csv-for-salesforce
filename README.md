prepare-csv-for-salesforce

## Debug

```
git clone https://gitlab.com/love_intent/prepare-csv-for-salesforce
cd prepare-csv-for-salesforce
yarn link
yarn start
```

## Publish

```
yarn publish --access public
```

## Install

```
yarn add @next_leve1/prepare-csv-for-salesforce
```

## Usage

CLI

```
prepare-csv-for-salesforce ./relative/path/to/source.csv ./relative/path/to/output.json
```

Or

```
import fs from 'fs'
import path from 'path'
import prepare from '@next_leve1/prepare-csv-for-salesforce'

const csvData = fs.readFileSync(path.resolve(__dirname, './relative/path/to/source.csv'), 'utf8')

const data = prepare.sync(csvData)
```
