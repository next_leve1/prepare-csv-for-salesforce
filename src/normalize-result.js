import _dropRight from 'lodash.dropright'
import _groupBy from 'lodash.groupby'

export default (result, { groupBy, primaryKeys, duplicateKeys }) => {
  const items = _dropRight(result)
  const itemsGroupedBy = _groupBy(items, groupBy)

  const normalizedItems = Object.keys(itemsGroupedBy).map((groupedItemsKey) => {
    const normalizedItem = {}
    const { [groupedItemsKey]: groupedItems } = itemsGroupedBy
    const activeItem = groupedItems.find(
      (groupedItem) => groupedItem.contractActivationDate && groupedItem.contractEndDate,
    )
    const notActiveItems = groupedItems.filter(
      (groupedItem) => !groupedItem.contractActivationDate && !groupedItem.contractEndDate,
    )

    primaryKeys.forEach((primaryKey) => {
      normalizedItem[primaryKey] = activeItem[primaryKey]
    })

    duplicateKeys.forEach((duplicateKey) => {
      normalizedItem[duplicateKey] = [activeItem[duplicateKey]]

      notActiveItems.forEach((notActiveItem) => {
        normalizedItem[duplicateKey].push(notActiveItem[duplicateKey])
      })
    })

    return normalizedItem
  })

  return normalizedItems
}
