import fs from 'fs'
import stream from 'stream'
import path from 'path'
import csvParse from 'csv-parse/lib/sync'
import normalizeResult from '@/normalize-result'
import constants from '@/constants'
import log from '@/utils/log'

const orderedColumnsKeys = [
  constants.columnsMap['Wireless number'],
  constants.columnsMap['Account number'],
  constants.columnsMap['Billing cycle date'],
  constants.columnsMap['Cost center'],
  constants.columnsMap['User name'],
  constants.columnsMap['Contract activation date'],
  constants.columnsMap['Contract end date'],
  constants.columnsMap['Upgrade eligibility date'],
  constants.columnsMap['Account charges'],
  constants.columnsMap['Equipment charges'],
  constants.columnsMap['Monthly access charges'],
  constants.columnsMap['Other charges & credits'],
  constants.columnsMap['Taxes and surcharges'],
  constants.columnsMap['Purchase charges'],
  constants.columnsMap['Total current charges'],
  constants.columnsMap['Long distance/Other Charges'],
  constants.columnsMap['Data usage'],
  constants.columnsMap['Total data usage charges'],
  constants.columnsMap['Messaging charges'],
  constants.columnsMap['Mobile to mobile minutes'],
  constants.columnsMap['Roaming charges'],
  constants.columnsMap['Nights and weekends (minutes)'],
  constants.columnsMap['Plan usage (minutes)'],
  constants.columnsMap['International charges'],
]

const parserOptions = {
  from_line: 15,
  relax: true,
  relax_column_count: true,
  trim: true,
  columns: orderedColumnsKeys,
}

export const sync = (source) => {
  try {
    const result = csvParse(source, parserOptions)

    const normalizedResult = normalizeResult(result, {
      groupBy: constants.columnsMap['Account number'],
      primaryKeys: constants.primaryKeys,
      duplicateKeys: constants.duplicateKeys,
    })

    return normalizedResult
  } catch (error) {
    log.error(error)

    throw new Error(constants.UNKOWN_ERROR_MESSAGE)
  }
}

export default {
  sync,
}
