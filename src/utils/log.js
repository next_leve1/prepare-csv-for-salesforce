export const error = (...args) => {
  console.log('\n\x1b[31m%s\x1b[0m', 'error', ...args)
}

export const info = (...args) => {
  console.log('\n\x1b[32m%s\x1b[0m', 'info', ...args)
}

export const speed = (...primaryArgs) => {
  const startTime = Date.now()

  if (primaryArgs.length) {
    console.log('\n\x1b[33m%s\x1b[0m', 'speed', ...primaryArgs, 'is processing')
  }

  return (...secondaryArgs) => {
    const endTime = Date.now()
    const difference = endTime - startTime

    console.log(
      '\n\x1b[33m%s\x1b[0m',
      'speed',
      ...primaryArgs,
      ...secondaryArgs,
      'completed in',
      `${difference}ms`,
    )
  }
}

export default {
  error,
  info,
  speed,
}
