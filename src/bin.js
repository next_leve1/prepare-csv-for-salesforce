import fs from 'fs'
import path from 'path'
import lib from '@/index'
import constants from '@/constants'
import log from '@/utils/log'

const {
  argv: [, , sourcePath, resultPath],
} = process

if (typeof sourcePath !== 'string' || typeof resultPath !== 'string') {
  throw Error(constants.PATH_ERRORS_MESSAGE(sourcePath, resultPath))
}

try {
  const sourceFullPath = path.resolve(sourcePath)
  const resultFullPath = path.resolve(resultPath)

  log.info(`CSV source ${sourceFullPath} is processing`)

  const csvData = fs.readFileSync(sourceFullPath, 'utf8')

  const parsingSpeedEndLog = log.speed('Parse')

  const data = lib.sync(csvData)

  parsingSpeedEndLog()

  fs.writeFileSync(path.resolve(resultFullPath), JSON.stringify(data, null, 2))

  log.info(`JSON result saved in ${resultFullPath}`)
} catch (error) {
  log.error(error)

  if (error.code === 'ENOENT') {
    throw Error(constants.FILE_NOT_EXIST_MESSAGE(sourcePath))
  }

  throw error
}
