export const PATH_ERRORS_MESSAGE = (sourcePath, resultPath) =>
  `Expected a .csv source relative path as a first argument, got "${sourcePath}". And a relative path for a .json result, got "${resultPath}"`

export const FILE_NOT_EXIST_MESSAGE = (filePath) => `File ${filePath} doesn't exist`

export const UNKOWN_ERROR_MESSAGE = 'Unknown error'

export const primaryColumnsMap = {
  'Account number': 'accountNumber',
  'Wireless number': 'wirelessNumber',
  'User name': 'userName',
  'Contract activation date': 'contractActivationDate',
  'Contract end date': 'contractEndDate',
}

export const duplicateColumnsMap = {
  'Upgrade eligibility date': 'upgradeEligibilityDate',
  'Cost center': 'costCenter',
  'Billing cycle date': 'billingCycleDate',
  'Account charges': 'accountCharges',
  'Equipment charges': 'equipmentCharges',
  'Monthly access charges': 'monthlyAccessCharges',
  'Other charges & credits': 'otherChargesAndCredits',
  'Taxes and surcharges': 'taxesAndSurcharges',
  'Total current charges': 'totalCurrentCharges',
  'Long distance/Other Charges': 'longDistanceAndOtherCharges',
  'Total data usage charges': 'totalDataUsageCharges',
  'Messaging charges': 'messagingCharges',
  'Mobile to mobile minutes': 'mobileToMobileMinutes',
  'Roaming charges': 'roamingCharges',
  'Nights and weekends (minutes)': 'nightsAndWeekendsMinutes',
  'Plan usage (minutes)': 'planUsageMinutes',
  'International charges': 'internationalCharges',
  'Purchase charges': 'purchaseCharges',
}

export const otherColumnsMap = {
  'Data usage': 'dataUsage',
}

export const columnsMap = {
  ...primaryColumnsMap,
  ...duplicateColumnsMap,
  ...otherColumnsMap,
}

export const primaryKeys = Object.values(primaryColumnsMap)

export const duplicateKeys = Object.values(duplicateColumnsMap)

export const otherKeys = Object.values(otherColumnsMap)

export const columnsKeys = Object.values(columnsMap)

export default {
  PATH_ERRORS_MESSAGE,
  FILE_NOT_EXIST_MESSAGE,
  UNKOWN_ERROR_MESSAGE,
  primaryColumnsMap,
  duplicateColumnsMap,
  otherColumnsMap,
  columnsMap,
  primaryKeys,
  duplicateKeys,
  columnsKeys,
}
