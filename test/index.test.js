import fs from 'fs'
import path from 'path'
import prepare from '@/index'
import { assert } from 'chai'

describe('parsing', () => {
  it('parsing successfull', async () => {
    const csvData = fs.readFileSync(path.resolve(__dirname, './mocks/source.csv'), 'utf8')

    const data = prepare.sync(csvData)

    assert.isArray(data)

    const [item] = data

    assert.isString(item.accountNumber)
    assert.isString(item.wirelessNumber)
    assert.isString(item.userName)
    assert.isString(item.contractActivationDate)
    assert.isString(item.contractEndDate)
    assert.isArray(item.upgradeEligibilityDate)
    assert.isArray(item.costCenter)
    assert.isArray(item.billingCycleDate)
    assert.isArray(item.accountCharges)
    assert.isArray(item.equipmentCharges)
    assert.isArray(item.monthlyAccessCharges)
    assert.isArray(item.otherChargesAndCredits)
    assert.isArray(item.taxesAndSurcharges)
    assert.isArray(item.totalCurrentCharges)
    assert.isArray(item.longDistanceAndOtherCharges)
    assert.isArray(item.totalDataUsageCharges)
    assert.isArray(item.messagingCharges)
    assert.isArray(item.mobileToMobileMinutes)
    assert.isArray(item.roamingCharges)
    assert.isArray(item.nightsAndWeekendsMinutes)
    assert.isArray(item.planUsageMinutes)
    assert.isArray(item.internationalCharges)
    assert.isArray(item.purchaseCharges)
  })
})
