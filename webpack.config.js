import path from 'path'
import webpack from 'webpack'
import webpackNodeExternals from 'webpack-node-externals'
import config from '@/config'

const plugins = [new webpack.BannerPlugin({ banner: '#!/usr/bin/env node', raw: true })]

if (config.isDevelopment) {
}

export default {
  target: 'node',
  context: __dirname,
  entry: {
    index: './src/index.js',
    bin: './src/bin.js',
  },
  output: {
    path: path.resolve(__dirname, './dist'),
    filename: '[name].js',
  },
  module: {
    rules: [
      {
        test: /\.js$/,
        use: ['babel-loader'],
        exclude: /node_modules/,
      },
    ],
  },
  plugins,
  externals: [webpackNodeExternals()],
  mode: config.isProduction ? 'production' : 'development',
}
